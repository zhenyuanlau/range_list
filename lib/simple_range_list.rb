# frozen_string_literal: true

require_relative 'simple/mergable_range_builder'
require_relative 'simple/merged_range_builder'

#--
# The implementations of `add`/`remove` methods following the same pattern:
# - looking up the mergable ranges according to the ranges of RangeList and range to add or remove
# - building ranges need to push by the mergable ranges and range to add or remove
# - sortging and merging the ranges of RangeList
#++

# A simple implement of RangeList
class SimpleRangeList < RangeList
  def initialize(ranges = [])
    super()
    @ranges = ranges
  end

  def add(range)
    range = Range(range)

    return if range.invalid?

    (@ranges << range) && return if @ranges.empty?

    mergable_ranges = AddMergableRangeBuilder.mergable_ranges(@ranges, range)

    @ranges = AddMergedRangeBuilder.range_list(@ranges, mergable_ranges, range)

    sort_and_merge!
  end

  def remove(range)
    range = Range(range)

    return if range.invalid? || @ranges.empty?

    mergable_ranges = RemoveMergableRangeBuilder.mergable_ranges(@ranges, range)

    @ranges = RemoveMergedRangeBuilder.range_list(@ranges, mergable_ranges, range)

    sort_and_merge!
  end

  def print
    puts "Should display: #{inspect}"
  end

  def to_s
    @ranges.map(&:to_s).join(' ')
  end

  private

  def sort_and_merge!
    @ranges.sort_by!(&:first)

    merge
  end

  def merge
    i = @ranges.length - 1
    until i == 1
      first = @ranges[i].begin
      last = @ranges[i - 1].end
      do_merge(i) if last == first - 1
      i -= 1
    end
  end

  def do_merge(pos)
    @ranges[pos - 1] = Range([@ranges[pos - 1].begin, @ranges[pos].end])
    @ranges.delete_at(pos)
  end
end
