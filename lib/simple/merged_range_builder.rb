# frozen_string_literal: true

# MergedRangeBuilder Interface
class MergedRangeBuilder
  class << self
    def range_list(ranges, mergable_ranges, range)
      @instance ||= new
      @instance.range_list(ranges, mergable_ranges, range)
    end
  end

  def range_list(ranges, mergable_ranges, range)
    raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
  end
end

# MergedRangeBuilder for add
class AddMergedRangeBuilder < MergedRangeBuilder
  def range_list(ranges, mergable_ranges, range)
    dup_ranges = ranges.dup

    if mergable_ranges.empty?
      merge_when_mergable_ranges_empty(dup_ranges, range)
    else
      merge_when_mergable_ranges_any(dup_ranges, mergable_ranges, range)
    end
    dup_ranges
  end

  private

  def merge_when_mergable_ranges_empty(dup_ranges, range)
    dup_ranges << range
  end

  def merge_when_mergable_ranges_any(dup_ranges, mergable_ranges, range)
    mergable_ranges << range
    min = mergable_ranges.map(&:first).min
    max = mergable_ranges.map(&:last).max
    mergable_ranges.each do |mr|
      dup_ranges.delete(mr)
    end
    dup_ranges << Range([min, max])
  end
end

# MergedRangeBuilder for remove
class RemoveMergedRangeBuilder < MergedRangeBuilder
  def range_list(ranges, mergable_ranges, range)
    dup_ranges = ranges.dup
    if mergable_ranges.any?
      mergable_ranges.each do |mergable_range|
        merge_when_range_at_middle(dup_ranges, mergable_range, range)
        merge_when_range_at_left(dup_ranges, mergable_range, range)
        merge_when_range_at_right(dup_ranges, mergable_range, range)
        dup_ranges.delete(mergable_range)
      end
    end
    dup_ranges
  end

  private

  def merge_when_range_at_middle(dup_ranges, mergable_range, range_to_add)
    return unless range_to_add.begin > mergable_range.begin && range_to_add.end <= mergable_range.end

    dup_ranges << Range([mergable_range.begin, range_to_add.begin])
    dup_ranges << Range([range_to_add.end, mergable_range.end])
  end

  def merge_when_range_at_left(dup_ranges, mergable_range, range_to_add)
    return unless range_to_add.begin <= mergable_range.begin && range_to_add.end < mergable_range.end

    dup_ranges << Range([range_to_add.end, mergable_range.end])
  end

  def merge_when_range_at_right(dup_ranges, mergable_range, range_to_add)
    return unless range_to_add.begin >= mergable_range.begin && range_to_add.end >= mergable_range.end

    dup_ranges << Range([mergable_range.begin, range_to_add.begin])
  end
end
