# frozen_string_literal: true

# MergableRangeBuilder Interface
class MergableRangeBuilder
  class << self
    def mergable_ranges(ranges, range)
      @instance ||= new
      @instance.mergable_ranges(ranges, range)
    end
  end

  def mergable_ranges(ranges, range)
    raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
  end
end

# MergableRangeBuilder for add
class AddMergableRangeBuilder < MergableRangeBuilder
  def mergable_ranges(ranges, range_to_add)
    mergable_ranges = []
    ranges.each do |range|
      next if range.begin == range_to_add.begin && range.end == range_to_add.end

      do_merge(mergable_ranges, range, range_to_add)
    end
    mergable_ranges
  end

  private

  def do_merge(mergable_ranges, range, range_to_add)
    if mergable_ranges.any?
      merge_when_mergable_ranges_any(mergable_ranges, range, range_to_add)
    else
      merge_when_mergable_ranges_empty(mergable_ranges, range, range_to_add)
    end
  end

  def merge_when_mergable_ranges_any(mergable_ranges, range, range_to_add)
    mergable_ranges << range if range_to_add.end >= range.begin - 1
  end

  def merge_when_mergable_ranges_empty(mergable_ranges, range, range_to_add)
    mergable_ranges << range if range_to_add.begin >= range.begin - 1 && range_to_add.begin <= range.end + 1
    mergable_ranges << range if range.begin >= range_to_add.begin && range.end <= range_to_add.end
  end
end

# MergableRangeBuilder for remove
class RemoveMergableRangeBuilder < MergableRangeBuilder
  def mergable_ranges(ranges, range_to_add)
    mergable_ranges = []
    ranges.each do |range|
      next if range.begin >= range_to_add.end || range.end <= range_to_add.begin

      mergable_ranges << range
    end
    mergable_ranges
  end
end
