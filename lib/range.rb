# frozen_string_literal: true

#--
# Open Range class adapt to the following definitions:
# A pair of integers define a range, for example: [1, 5). This range includes integers: 1, 2, 3, and 4.
#++

class InitializeError < StandardError; end

# Open Range class
class Range
  class << self
    def from_array(array)
      raise InitializeError if array.length != 2

      new(array.first, array.last, true)
    end
  end

  def invalid?
    first == last
  end

  def to_s
    "[#{first}, #{last})"
  end
end

# rubocop:disable Naming/MethodName
# Kernel methods:
#   - Range
module Kernel
  def Range(array)
    Range.from_array(array)
  end
end
# rubocop:enable Naming/MethodName
