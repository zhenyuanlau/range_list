Given('list is {string}') do |ranges_string|
  ranges = eval(ranges_string).map { |array| Range(array) }
  @list = SimpleRangeList.new(ranges)
end

When('send {string}.\({string}) to list') do |op_string, range_string|
  op = op_string.to_sym
  range = eval(range_string)
  @list.send(op, range)
end

Then('should display {string}') do |inspect_string|
  expect(@list.to_s).to eq(inspect_string)
end
