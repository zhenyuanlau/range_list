Feature: RangeList
  Implement a class named 'RangeList'

  Scenario Outline: RangeList
    Given list is "<ranges>"
    When  send "<op>".("<range>") to list
    Then  should display "<inspect>"

  Examples:
    | ranges                       | op     | range    | inspect                  |
    | []                           | add    | [1, 5]   | [1, 5)                   |
    | [[1, 5]]                     | add    | [10, 20] | [1, 5) [10, 20)          |
    | [[1, 5], [10, 20]]           | add    | [20, 20] | [1, 5) [10, 20)          |
    | [[1, 5], [10, 20]]           | add    | [20, 21] | [1, 5) [10, 21)          |
    | [[1, 5], [10, 21]]           | add    | [2, 4]   | [1, 5) [10, 21)          |
    | [[1, 5], [10, 21]]           | add    | [3, 8]   | [1, 8) [10, 21)          |
    | [[1, 8], [10, 21]]           | remove | [10, 10] | [1, 8) [10, 21)          |
    | [[1, 8], [10, 21]]           | remove | [10, 11] | [1, 8) [11, 21)          |
    | [[1, 8], [11, 21]]           | remove | [15, 17] | [1, 8) [11, 15) [17, 21) |
    | [[1, 8], [11, 15], [17, 21]] | remove | [3, 19]  | [1, 3) [19, 21)          |
