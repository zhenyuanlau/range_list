# RangeList

Reading *features/range_list.feature* file first, it's an executable document for RangeList.

## BDD

Writing tests with `cucumber scenario outline` in `features/range_list.feature` file.

There is some special cases need to consider, such as range boundaries when to add or remove a range.

I have only written tests according meet the basic requirements.

```bash
Feature: RangeList
  Implement a class named 'RangeList'

  Scenario Outline: RangeList
    Given list is "<ranges>"
    When  send "<op>".("<range>") to list
    Then  should display "<inspect>"

  Examples:
    | ranges                       | op     | range    | inspect                  |
    | []                           | add    | [1, 5]   | [1, 5)                   |
    | [[1, 5]]                     | add    | [10, 20] | [1, 5) [10, 20)          |
    | [[1, 5], [10, 20]]           | add    | [20, 20] | [1, 5) [10, 20)          |
    | [[1, 5], [10, 20]]           | add    | [20, 21] | [1, 5) [10, 21)          |
    | [[1, 5], [10, 21]]           | add    | [2, 4]   | [1, 5) [10, 21)          |
    | [[1, 5], [10, 21]]           | add    | [3, 8]   | [1, 8) [10, 21)          |
    | [[1, 8], [10, 21]]           | remove | [10, 10] | [1, 8) [10, 21)          |
    | [[1, 8], [10, 21]]           | remove | [10, 11] | [1, 8) [11, 21)          |
    | [[1, 8], [11, 21]]           | remove | [15, 17] | [1, 8) [11, 15) [17, 21) |
    | [[1, 8], [11, 15], [17, 21]] | remove | [3, 19]  | [1, 3) [19, 21)          |
```

## Semantic Commit

Using Semantic Commit Messages such like *type(scope): subject* for git commit, like a day job.

```bash
npm install -g commitizen

commitizen init cz-conventional-changelog --save-dev --save-exact
```

## CI/CD

Executing `cucumber` for testing, `rubocop` for formatting in Gitlab CI.

```bash
bundle exec rubocop lib
bundle exec rspec spec
bundle exec cucumber
```

`Rspec` execute a [RSpec::Benchmark](https://github.com/piotrmurach/rspec-benchmark) test.

## Coding

Maybe using there is better algorithm for RangeList add/remove.

But I have no time to try because of focussing on making the code better in the Ruby way. You can extends RangeList to providing a better implementation.

```bash
lib
├── range.rb
├── range_list.rb
├── simple
│   ├── mergable_range_builder.rb
│   └── merged_range_builder.rb
└── simple_range_list.rb

1 directory, 5 files

```

I write the main code in `lib` directory.

The `simple_range_list.rb` is a simple implementation of `RangeList`.

The implementations of `add`/`remove` methods following the same pattern:
- looking up the mergable ranges according to the ranges of RangeList and range to add or remove
- building ranges need to push by the mergable ranges and range to add or remove
- sorting and merging the ranges of RangeList

The dependencies of `simple_range_list.rb` written in the `simple` directory.

By the way, `Range` in `range.rb` is the class of Ruby, I open it for RangeList.

## ToDo

[ ] Improve performance(optimize `SimpleRangeList` or providing a better implementation)

[ ] Refine testing(more boundary case)

[ ] Exception processing(more exception processing to consider)

## Thanks

I enjoyed the trip.
