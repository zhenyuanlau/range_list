# frozen_string_literal: true

require_relative 'spec_helper'
require_relative '../lib/range'
require_relative '../lib/range_list'
require_relative '../lib/simple_range_list'

RSpec.describe 'Performance testing' do
  include RSpec::Benchmark::Matchers

  # | [[1, 8], [11, 21]]           | remove | [15, 17] | [1, 8) [11, 15) [17, 21) |
  let!(:ranges) { [[1, 8], [11, 21]].map { |range| Range(range) } }
  let!(:list) { SimpleRangeList.new(ranges) }
  let!(:range) { [15, 17] }

  it 'test' do
    expect do
      list.remove(range)
    end.to perform_under(5).ms
  end
end
